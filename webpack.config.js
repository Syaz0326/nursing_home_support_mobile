const webpack = require("webpack")
const path = require("path")
const VueLoaderPlugin = require("vue-loader/lib/plugin")

module.exports = {
  entry: path.resolve(__dirname, 'www/src/main.js'), //エントリーポイント
  output: {
    path: path.resolve(__dirname, 'www/bundle/'), //outputディレクトリ
    filename: 'bundle.js' //filename
  },
  module: { //トランスパイルする拡張子とローダの設定
    rules: [ //test: 拡張子指定 e.g. /\.txt/, use: ローダ指定 e.g. raw-loader
      {test: /\.vue$/, use: 'vue-loader'},
      {test: /\.js$/, use: 'babel-loader'},
      {test: /\.css$/, use: ['vue-style-loader', 'style-loader', 'css-loader']},
      {test: /\.scss$/, use: ['vue-style-loader', 'css-loader', 'sass-loader', {
        loader: 'sass-resources-loader',
        options: {
          resources: path.resolve(__dirname, 'www/scss/common.scss')
        }
      }]},
      {test: /\.(ttf|eot|svg)$/, use: 'file-loader'},
      {test: /\.woff$/, use: 'url-loader'},
      {test: /\.woff2$/, use: 'url-loader'}
    ]
  },
  plugins: [ //プラグイン
    new VueLoaderPlugin()
  ],
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      vue: 'vue/dist/vue'
    }
  }
}
