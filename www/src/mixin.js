import Vue from 'vue'
import {mapState, mapMutations, mapActions} from 'vuex'
import axios from 'axios'

let mixin = {
  computed: mapState({
    resident: state => state.resident
  }),
  methods: {
    ...mapActions([
      'getAllResidents',
      'getAllRooms',
      'getResidentData',
      'dataPost'
    ]),
    logData() {
      console.log(this.$store.state.postData)
    }
  }
}

module.exports = mixin
