import axios from 'axios'
import moment from 'moment'
import uuid from 'uuid'
const uuidv4 = uuid.v4

let localStorage = window.localStorage

const numberInputProp = {
  bloodPresureTop: 'top',
  bloodPresureBottom: 'bottom',
  bodyTemperature: 'temperature',
  dosage: 'dose',
  hydrate: 'intake',
  mealStaple: 'staple_food',
  mealSide: 'side_dish',
  urinate: 'volume'
}

const listSelectProp = {
  hydrate: 'kind',
  defecationSize: 'volume',
  defecationColor: 'color',
  defecationForm: 'form',
  dosage: 'kind'
}

const categories = {
  blood_pressure: {
    category: '血圧',
    link: 'blood-pressure'
  },
  body_temperature: {
    category: '体温',
    link: 'body-temperature'
  },
  meal: {
    category: '食事',
    link: 'meal'
  },
  hydrate: {
    category: '水分',
    link: 'hydrate'
  },
  defecation: {
    category: '排便',
    link: 'defecation'
  },
  dosage: {
    category: '投薬',
    link: 'dosage'
  },
  urinate: {
    category: '排尿',
    link: 'urinate'
  }
}

const commonTemplate = {
  id: null,
  resident_id: null,
  created_at: null,
  updated_at: null,
  staff_id: null
}

const templates = {
  BloodPressure() {
    return Object.assign({
      top: null,
      bottom: null
    }, commonTemplate)
  },
  BodyTemperature() {
    return Object.assign({
      temperature: null
    }, commonTemplate)
  },
  Meal() {
    return Object.assign({
      staple_food: null,
      side_dish: null
    }, commonTemplate)
  },
  Hydrate() {
    return Object.assign({
      kind: null,
      intake: null
    }, commonTemplate)
  },
  Defecation() {
    return Object.assign({
      volume: null,
      color: null,
      form: null
    }, commonTemplate)
  },
  Dosage() {
    return Object.assign({
      kind: null,
      dose: null
    }, commonTemplate)
  },
  Urinate() {
    return Object.assign({
      volume: null
    }, commonTemplate)
  }
}

// store
let state = {
  resident: {
    id: '',
    name: '',
  },
  category: '',
  postData: {},
  savedData: {},
  isNewData: true,
  staff: {
    id: '',
    name: ''
  },
  residentsList: [],
  rooms: [],
  focusInputType: null,
  numberInputProp: numberInputProp,
  listSelectProp: listSelectProp,
  HOST: localStorage.getItem('HOST') || 'localhost',
  PORT: localStorage.getItem('PORT') || '0000',
  comProtocol: 'http://'
}

let getters = {
  filteringResidents: (state) => (number) => {
    if (number === 'all') {
      return state.residentsList
    } else {
      number = parseInt(number)
      return state.residentsList.filter(({room_number}) => room_number === number)
    }
  },
  getNumberValue: (state) => (prop) => {
    return state.postData[prop]
  },
  getSelectedValue : (state) => (prop) => {
    return state.postData[prop]
  },
  filteringPostData: (state) => {
    let returnValue = {}
    let invalidKeys = ['id', 'resident_id', 'created_at', 'updated_at', 'staff_id']
    let keys = Object.keys(state.postData).filter(key => !invalidKeys.includes(key))
    for (let key of keys) {
      returnValue[key] = state.postData[key]
    }
    return returnValue
  },
  filteringSavedData: (state) => {
    let returnValue = {}
    let invalidKeys = ['id', 'resident_id', 'created_at', 'updated_at', 'staff_id']
    let keys = Object.keys(state.savedData).filter(key => !invalidKeys.includes(key))
    for (let key of keys) {
      returnValue[key] = state.savedData[key]
    }
    return returnValue
  },
  getIPArray: (state) => () => {
    let array = state.HOST.split('.')
    return array
  },
  getIPOct: (state) => (octOrder) => {
    let array = state.HOST.split('.')
    return array[octOrder]
  }
}

let actions = {
  getAllResidents({ state, commit }) {
    return axios.get(state.comProtocol + state.HOST + ':' + state.PORT + '/get-all-residents')
    .then((res) => {
      commit('setResidents', res.data)
    })
  },
  getAllRooms({ state, commit }) {
    return axios.get(state.comProtocol + state.HOST + ':' + state.PORT + '/get-all-rooms')
    .then((res) => {
      commit('setRooms', res.data)
    })
  },
  getResidentData({ state, commit }, resident_id) {
    return axios.get(state.comProtocol + state.HOST + ':' + state.PORT + '/get-resident-data?resident_id=' + resident_id)
    .then(({ data }) => {
      for (let datum of data) {
        datum.category = categories[datum.type].category
        datum.link = categories[datum.type].link
      }
      return data
    })
  },
  getData({ state, commit }, payload) {
    return axios.get(state.comProtocol + state.HOST + ':' + state.PORT + '/get-' + payload.link + '?id=' + payload.id)
    .then(({ data }) => {
      commit('setPostData', data)
      commit('setSavedData', data)
    })
  },
  dataSend({ state, dispatch }, query) {
    if(state.isNewData) {
      return dispatch('dataPost', query)
    } else {
      return dispatch('dataPut', query)
    }
  },
  async dataPost({ commit, dispatch, state }, query) {
    await dispatch('preparePost')
    return dispatch('post' + query.charAt(0).toUpperCase() + query.slice(1))
    // post成功処理
    .then(() => {
      commit('resetPostData')
      commit('resetNewFlag')
      return Promise.resolve()
    })
    // postエラー処理
    .catch(() => {
      let savedData = JSON.parse(localStorage.getItem('post_failed_data'))
      let saveData = state.postData
      saveData.category = query.charAt(0).toUpperCase() + query.slice(1)
      if(savedData === null) {
        savedData = [saveData]
      } else {
        savedData.push(saveData)
      }
      localStorage.setItem('post_failed_data', JSON.stringify(savedData))
      return Promise.reject()
    })
  },
  async retryPost({ commit, dispatch }, failedData) {
    for(let item of failedData) {
      let query = item.category
      delete item.category
      commit('retryPostData', item)
      dispatch('post' + query)
      .catch(() => {
        console.log(error)
      })
    }
    return Promise.resolve()
  },
  postBloodPressure({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-blood-pressure', state.postData)
  },
  postBodyTemperature({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-body-temperature', state.postData)
  },
  postMeal({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-meal', state.postData)
  },
  postHydrate({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-hydrate', state.postData)
  },
  postDefecation({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-defecation', state.postData)
  },
  postDosage({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-dosage', state.postData)
  },
  postUrinate({ state }) {
    return axios.post(state.comProtocol + state.HOST + ':' + state.PORT + '/post-urinate', state.postData)
  },
  preparePost({ commit, state }) {
    return Promise.resolve()
    .then(() => {
      commit('setResidentIdToData')
      commit('setIdToData')
    })
  },
  async dataPut({ commit, dispatch, state }, query) {
    let putData = await dispatch('preparePut')
    return await dispatch('put' + query.charAt(0).toUpperCase() + query.slice(1), putData)
    .then(() => {
      commit('resetPostData')
      commit('resetNewFlag')
    })
  },
  putBloodPressure({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-blood-pressure', putData)
  },
  putBodyTemperature({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-body-temperature', putData)
  },
  putMeal({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-meal', putData)
  },
  putHydrate({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-hydrate', putData)
  },
  putDefecation({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-defecation', putData)
  },
  putDosage({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-dosage', putData)
  },
  putUrinate({ state }, putData) {
    return axios.put(state.comProtocol + state.HOST + ':' + state.PORT + '/put-urinate', putData)
  },
  preparePut({ commit, dispatch }) {
    return dispatch('compareData')
    .then((putData) => {
      putData.updated_at = moment().format()
      return putData
    })
  },
  // 変更前データと変更後データを比較し変更されたデータと必須データを残したデータを返却する
  compareData({ state }) {
    let newData = state.postData
    let oldData = state.savedData
    let returnValue = {}

    for (let key of Object.keys(newData)) {
      if(newData[key] !== oldData[key] || key === 'id' || key === 'resident_id' || key === 'update_at' || key === 'kind') {
        returnValue[key] = newData[key]
      }
    }

    return returnValue
  },
  // HOSTのlocalStorageへの保存
  saveHOST({ commit }, HOST) {
    localStorage.setItem('HOST', HOST)
    commit('setHOST', HOST)
  },
  // PORTのlocalStorageへの保存
  savePORT({ commit }, PORT) {
    localStorage.setItem('PORT', PORT)
    commit('setPORT', PORT)
  }
}

let mutations = {
  setResidents: (state, residents) => state.residentsList = residents,
  focusResident: (state, resident) => state.resident = resident,
  focusStaff: (state, staff) => state.staff = staff,
  focusCategory: (state, cate) => state.category = cate,
  increment: (state, payload) => state.postData[payload.prop] += payload.value,
  decrement: (state, payload) => state.postData[payload.prop] -= payload.value,
  overwriteValue: (state, payload) => state.postData[payload.prop] = payload.value,
  setRooms: (state, rooms) => state.rooms = rooms,
  setInputType: (state, payload) => state.focusInputType = payload.type,
  setPostTemplate: state => state.postData = templates[state.focusInputType](),
  setIdToData: state => state.postData.id = uuidv4(),
  setCreateDate: (state, payload) => state.postData.created_at = payload.created_at,
  setUpdateDate: state => state.postData.updated_at = moment().format(),
  setResidentIdToData: state => state.postData.resident_id = state.resident.id,
  resetPostData: state => state.postData = {},
  resetNewFlag: state => state.isNewData = true,
  setPostData: (state, data) => state.postData = {...data},
  setSavedData: (state, data) => state.savedData = {...data},
  changeNewFlag: (state, isNewData) => state.isNewData = isNewData,
  setHOST: (state, HOST) => state.HOST = HOST,
  setPORT: (state, PORT) => state.PORT = PORT,
  retryPostData: (state, data) => state.postData = data
}

module.exports = {
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
}
