import Vue from "Vue"
import App from "./app/app"
import Vuex from 'vuex'
import VueRouter from 'vue-router'

// import onsenui
import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'
import VueOnsen from 'vue-onsenui'

// import font-awesome
import 'vue-awesome/icons/chevron-left'
import 'vue-awesome/icons/gear'
import Icon from 'vue-awesome/components/Icon'

import store from './store.js'
import mixin from './mixin.js'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueOnsen)
Vue.component('icon', Icon)
Vue.mixin(mixin)

import routes from './routes'
const router = new VueRouter({
  routes: routes
})

let vm = new Vue({
  store: new Vuex.Store(store),
  router,
  render(h) {
    return h(App)
  }
}).$mount('#app')
