import Top from './app/top'
import SelectOperation from './app/select_operation'
import ComponentTest from './components/componentTest'
import BloodPressure from './app/blood_pressure'
import Defecation from './app/defecation'
import BodyTemperature from './app/body_temperature'
import Meal from './app/meal'
import Hydrate from './app/hydrate'
import Dosage from './app/dosage'
import Urinate from './app/urinate'
import Confirmation from './app/confirmation'
import SetIP from './app/set_ip'

export default [
  {
    path: '/',
    component: Top
  },{
    path: '/select-operation/:id',
    component: SelectOperation
  },{
    path: '/blood-pressure/:id',
    component: BloodPressure
  },{
    path: '/body-temperature/:id',
    component: BodyTemperature
  },{
    path: '/meal/:id',
    component: Meal
  },{
    path: '/hydrate/:id',
    component: Hydrate
  },{
    path: '/defecation/:id',
    component: Defecation
  },{
    path: '/dosage/:id',
    component: Dosage
  },{
    path: '/urinate/:id',
    component: Urinate
  },{
    path: '/confirmation',
    component: Confirmation
  },{
    path: '/set-ip',
    component: SetIP
  },{
    path: '/test',
    component: ComponentTest
  }
]
