import Vue from 'vue'
import {mapState, mapMutations, mapActions} from 'vuex'
import axios from 'axios'

let mixin = {
  created() {
    this.$store.commit('setInputType', {type: this.type})
    this.$store.commit('setPostTemplate')
  }
}

module.exports = mixin
