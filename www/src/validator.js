import Joi from 'joi'

//* テストデータ
const HydrateTypes = ['ヤクルト', 'お茶', '水', '点滴', 'ポカリ']

const DefecationSize = ['極小', '小', '中', '大']
const DefecationColor = ['淡黄', '黄褐', '茶', '褐色', '灰', '黒']
const DefecationForm = ['コロコロ', 'バナナ', '泥状']

const DosageType = ['ラキソベロン', 'プルゼニド']
//*/

const commonShema = {
  id: Joi.string().required(),
  resident_id: Joi.string().required(),
  created_at: Joi.string().required(),
  updated_at: Joi.string(),
  staff_id: Joi.string()
}

const PostDataSchemas = {
  BloodPressure: Joi.object().keys(Object.assign({
    top: Joi.number().integer().min(0).max(200).required(),
    bottom: Joi.number().integer().min(0).max(100).required(),
  }, commonShema)),
  BodyTemperature: Joi.object().keys(Object.assign({
    temperature: Joi.number().integer().min(30).max(40).required()
  }, commonShema)),
  Meal: Joi.object().keys(Object.assign({
    staple_food: Joi.number().integer().min(0).max(10).required(),
    side_dish: Joi.number().integer().min(0).max(10).required()
  }, commonShema)),
  Hydrate: Joi.object().keys(Object.assign({
    type: Joi.string().valid(HydrateTypes).required(),
    intake: Joi.number().required()
  }, commonShema)),
  Defecation: Joi.object().keys(Object.assign({
    size: Joi.string().valid(DefecationSize).required(),
    color: Joi.string().valid(DefecationColor).required()
    form: Joi.string().valid(DefecationForm).required()
  }, commonShema)),
  Dosage: Joi.object().keys(Object.assign({
    type: Joi.string().valiid(DosageType).required(),
    dose: Joi.number().required()
  }, commonShema)),
  Urinate: Joi.object().keys(Object.assign({
    volume: Joi.number().min(0).required()
  }))
}
